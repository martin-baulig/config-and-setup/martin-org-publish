;;; martin-org-publish.el --- Org Mode publish script -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 Martin Baulig
;;
;; Author: Martin Baulig <martin@baulig.is>
;; Maintainer: Martin Baulig <martin@baulig.is>
;; Created: April 28, 2023
;; Modified: April 28, 2023
;; Version: 0.0.1
;; Homepage: https://gitlab.com/martin-baulig/config-and-setup/martin-org-publish
;; Package-Requires: ((emacs "28.2"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;; Publish all of my Literate Code files.
;;
;;; Code:

(require 'ox-publish)

(defun martin-org-publish-prepare-project (props)
  (message "PREPARE: %s - %s" props (plist-get props :base-directory))
  (let* ((base-dir (plist-get props :base-directory))
         (exclude (plist-get props :exclude))
         (all-files (directory-files-recursively base-dir "\.org$")))
    (message "PREPARE #1: %s" all-files)
    (mapc (lambda (file)
            (message "PREPARE #2: %s - %s" file exclude)
            (unless (and exclude
                         (string-match-p exclude file))
              (message "PREPARE #3 - TANGLE %s" file)
              (org-babel-tangle-file file)
              (message "PREPARE #4 - TANGLE DONE")))
          all-files)))

(setq org-publish-project-alist
      '(("style"
         :base-directory "org-themes"
         :base-extension "org"
         :publishing-directory "public"
         :recursive t
         :exclude "readtheorg\\.org\\|ORIGINAL-README\\.org"
         :publishing-function org-html-publish-to-html
         :preparation-function martin-org-publish-prepare-project)
        ("doom-config"
         :base-directory "modules/doom-config"
         :base-extension "org"
         :publishing-directory "public"
         :recursive t
         :publishing-function org-html-publish-to-html
         :auto-sitemap t)
        ("static"
         :base-directory "org-themes"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf"
         :publishing-directory "public/assets"
         :recursive t
         :publishing-function org-publish-attachment)
        ("org" :components ("style" "static" "doom-config"))))

(provide 'martin-org-publish)
;;; martin-org-publish.el ends here
