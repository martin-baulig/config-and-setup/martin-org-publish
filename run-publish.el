(require 'martin-org-publish (expand-file-name "./martin-org-publish.el"))
(require 'htmlize)

(org-publish-all t)

;; Local Variables:
;; flycheck-disabled-checkers: '(emacs-lisp-package emacs-lisp-checkdoc)
;; End:
