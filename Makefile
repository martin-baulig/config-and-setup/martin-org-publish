EMACS	= emacs
SCRIPT	= run-publish.el

.PHONY:	 all clean

all::
	$(EMACS) --batch --script $(SCRIPT)

clean::
	git clean -xffd
